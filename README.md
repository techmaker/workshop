# What you need

All projects are made for **Software Workbench for STM32**. Latest version can be found here: http://www.openstm32.org/Downloading+the+System+Workbench+for+STM32+installer?structure=Documentation

You need nucleo-f446re board and some sensors/shields/boards depends on workshop

# Context

01 - SD Card and LCD Shields (8-bit interface). SD card connected by SPI.